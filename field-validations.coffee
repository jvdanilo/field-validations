app = angular.module 'field.validations', []

app.run ($locale)->
  $locale.VALIDATIONS = {}

  angular.extend $locale.VALIDATIONS, {
    required    : " The :attribute field is required."
    required_if : " The :attribute field is required when :other is :value."
    minlength   : " Must be at least :length characters."
    email       : " Must be a valid email."
    url         : " Must be a valid url."

    custom:
      'custom-field':
        required: 'Write down the :attribute'

    attributes: {
      'custom-field': 'Captcha'
    }
  }

app.service 'Validation', ($locale) ->
  instance = new class
    constructor: ->
      @validators = {}

    extend: (name, fn) ->
      @validators[name] = fn

    fieldName: (name) ->
      {VALIDATIONS} = $locale
      return VALIDATIONS.attributes[name] or do ->
        if name and name.length > 0
          return name.replace(/_/g, ' ')
        return name

    message: (validation, vars) ->
      {VALIDATIONS} = $locale

      friendly = instance.fieldName(vars.name)

      if VALIDATIONS.custom[vars.name] and VALIDATIONS.custom[vars.name][validation]
        messageText = VALIDATIONS.custom[vars.name][validation]
      else
        messageText = VALIDATIONS[validation]

      vars.attribute = friendly

      for k, v of vars
        regex = new RegExp '\:' + k + '', 'g'
        messageText = messageText.replace regex, v

      return messageText


  instance.extend 'required', (scope, elem, attrs, ngModel) ->
    return {
      msg: ->
        return instance.message 'required', {name: attrs.name}

      test: (value) ->
        if not value
          ngModel.$error.required = true;
          return false

        delete ngModel.$error.required;
        return true
    }

  instance.extend 'email', (scope, elem, attrs, ngModel) ->
    return {
      msg: ->
        return instance.message 'email', {name: attrs.name}

      test: (value) ->
        if not value
          return true
        regex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,20}$/i
        regex.test(value)
    }

  instance.extend 'minlength', (scope, elem, attrs, ngModel) ->
    return {
      msg: (length) ->
        return instance.message 'minlength', {name: attrs.name, length}

      test: (value, length) ->
        if value and value?.length < length
          ngModel.$error.minlength = length;
          return false
        return true
    }

  instance.extend 'required_if', (scope, elem, attrs, ngModel) ->
    newValue = null
    oldValue = null
    watchExpression = null
    comparation = 'truthy'
    started = false

    watchValue = (expression) ->
      return if started

      started = true

      parts = expression.split(',')
      [watchExpression, val] = parts

      if parts.length is 2
        comparation = val

      newValue = scope.$eval watchExpression

      scope.$watch watchExpression, (n, o) ->
        if n isnt o
          newValue = n
          oldValue = o
          ngModel.$validate()
        else
          newValue = n

    return {
      msg: (expression) ->
        return instance.message 'required_if', {
          name: attrs.name
          other: instance.fieldName(watchExpression)
          value: if comparation is 'truthy' then 'set' else comparation
        }

      test: (value, expression) ->
        watchValue(expression)

        if comparation is 'truthy'
          if newValue
            return false
          else
            return true
        else
          if newValue and newValue+'' is comparation+''
            if value
              return true
            else
              return false
          else
            return false

    }

  instance.extend 'url', (scope, elem, attrs, ngModel) ->
    return {
      msg: ->
        return instance.message 'url', {name: attrs.name}

      test: (value = "") ->
        `
        var pattern = new RegExp('^(https?:\\/\\/)' + // protocol
          '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
          '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
          '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
          '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
          '(\\#[-a-z\\d_\\/]*)?$', 'i'); // fragment locator
        `
        return true unless value

        if pattern.test(value)
          return true
        else
          return false
    }

  instance.extend 'error_object', (scope, elem, attrs, ngModel) ->

    parent_form = elem.parents('form')

    if attrs.hasOwnProperty('name')
      errors_from_elem = elem.parents('[errors-from]:first')
      if errors_from_elem.length > 0
        errors_from_elem.scope().$watch(errors_from_elem.attr('errors-from'), (n) ->
          ngModel.$validate()
        , true)

    return {
      fetch_object: ->
        errors_from_elem?.scope?()?.$eval(errors_from_elem.attr('errors-from')) or {}

      msg: ->
        errors_object = @fetch_object()
        property_error = errors_object[attrs.name]

        if property_error
          if angular.isArray(property_error)
            return property_error.join(" ")
          else
            return property_error

      test: (value) ->
        errors_object = @fetch_object()
        property_error = errors_object[attrs.name]
        if property_error? and property_error.length > 0
          setTimeout -> elem.trigger('show-message.validation')
          setTimeout -> parent_form?.trigger?('formTouched')
          return false
        return true
    }

  return instance


app.directive 'validate', ["$compile", "$locale", "Validation", ($compile, $locale, Validation) ->
  require: 'ngModel'
  link: (scope, elem, attrs, ngModel) ->
    inner_scope = scope.$new(true)
    inner_scope.ngModel = ngModel

    message = Validation.message

    validators = {}

    rule_names = attrs.validate.split("|")

    if attrs.hasOwnProperty('name')
      rule_names.push('error_object')

    validation_rules = {}
    for rule_name in rule_names then do (rule_names) ->
      [name, args] = rule_name.split(':')

      validation_rules[name] = args

      unless Validation.validators[name]
        return console.log "No validator #{name}"
        # throw new Error("No validator #{name}");

      validationDefinition = Validation.validators[name](scope, elem, attrs, ngModel)

      validators[name] = validationDefinition

      fn = (modelValue, viewValue) ->
        validationDefinition.test(modelValue or viewValue, args)

      if validationDefinition.async
        ngModel.$asyncValidators[name] = fn
      else
        ngModel.$validators[name] = fn

    form_group = elem.parents('.form-group:first')

    form_group_class = () ->
      setTimeout ->
        if inner_scope.$showMessage
          method = if $.isEmptyObject(ngModel.$error) then 'removeClass' else 'addClass'
          form_group[method]('has-error')

    inner_scope.$watch '$showMessage', (n) ->
      form_group_class()

    scope.$watch ->
      ngModel.$touched
    , (n, o) ->
      if n
        inner_scope.$showMessage = true

    elem.on 'show-message.validation', ->
      inner_scope.$showMessage = true

      inner_scope.$digest() unless inner_scope.$$phase

    parent_form = elem.parents('form')
    form_controller = parent_form.data('$formController')

    parent_form.on 'formTouched', ->
      inner_scope.$showMessage = true
      inner_scope.$digest() unless inner_scope.$$phase


    if form_controller
      scope.$watch ->
        form_controller.$submitted
      , (n, o) ->
        if n
          inner_scope.$showMessage = false

    scope.$watch attrs.ngModel, (n) ->
      if form_controller and form_controller.$submitted
        if n
          scope.$showMessage = true

    process_errors = (n) ->
      m = []
      for name of n
        args = validation_rules[name]
        if name is 'required' then continue
        if name is 'error_object' then continue

        m.push validators[name].msg args

      if n.required
        m.unshift validators.required.msg()

      inner_scope.$message = m.join('')

      if n.error_object
        inner_scope.$message = validators.error_object.msg()

      form_group_class()

    scope.$watch ->
      ngModel.$error
    , process_errors, true

    template = """
      <div class="help-block errors-message" ng-show="$showMessage && $message">
        {{$message}}
      </div>
    """

    delay = null;
    elem.on 'keydown keyup click', (e) ->
      unless ngModel.$touched
        setTimeout ->
          parent_form.trigger('formTouched')
          ngModel.$touched = true
          scope.$digest()
        , 500

    message_element = $compile(template)(inner_scope)
    message_element.insertAfter(elem)

]
